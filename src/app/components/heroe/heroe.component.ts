import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { HeroesService } from '../../servicios/heroes.service'


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
})
export class HeroeComponent {

  heroe: any = {}


  // Observador, es como una promesa, nos tenemos que subscribir
  // para escuchar esos cambios con parametros

  constructor( private activatedRoute: ActivatedRoute,
               private _heroesService: HeroesService) {
  // al pasar como parámetro el ['id'] solo logeamos el id del objeto
    this.activatedRoute.params.subscribe( params => {
      this.heroe = this._heroesService.getHeroe( params['id'])
    })
  }

}
