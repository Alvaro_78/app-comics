import { Component, OnInit } from '@angular/core';
import { Heroe, HeroesService } from '../../servicios/heroes.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
})
export class HeroesComponent implements OnInit {

  // Inicializamos la variable heroes con un array vacio
  heroes: Heroe[] = []

  constructor( private _heroesService: HeroesService,
               private router: Router  ) {

  }

  ngOnInit(){

    // this.heroes hace referencia al heroes del servicio
    // lo igualamos al heroesService con el método
    // y así nos traemos en el componente heroes el array heroes del servicio
    this.heroes = this._heroesService.getHeroes()
    // console.log(this.heroes)
  }


  // Con router redireccionamos la página según el path definido
  // el idx hace de parámetro
  verHeroe( idx:number ) {
    this.router.navigate( ['/heroe', idx] )
  }

}
