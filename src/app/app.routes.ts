// Aquí es donde importamos los componentes qque queremos enrutar y definimos
// en la const APP_ROUTES

import {  RouterModule, Routes } from '@angular/router'
import { HomeComponent } from './components/home/home.component'
import { HeroesComponent } from './components/heroes/heroes.component'
import { AboutComponent } from './components/about/about.component'
import { HeroeComponent } from './components/heroe/heroe.component'
import { BuscadorComponent } from './components/buscador/buscador.component'


const APP_ROUTES: Routes = [
  // definimos el path con el nombre del endpoint que tendrá
  { path: 'home', component: HomeComponent },
  { path: 'heroes', component: HeroesComponent },
  { path: 'about', component: AboutComponent },
  { path: 'heroe/:id', component: HeroeComponent },
  { path: 'buscar/:termino', component: BuscadorComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES)


